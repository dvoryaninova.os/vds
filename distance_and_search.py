from airport_data import Airport
from math import sin, cos, sqrt, atan2, radians

PARAMS = {
    'type': 0,
    'name': 1,
    'lat': 2,
    'lon': 3,
    'coords': slice(2, 4),
    'all': slice(0, 4)
}
RP = 6356751.8
RE = 6378136.5


def search(search_session, search_id, param='all'):
    try:
        searched_airport = search_session.query(Airport).filter_by(id=search_id).first()
        c_type, c_name, c_lat, c_lon = searched_airport.type, searched_airport.name, searched_airport.lat, \
                                       searched_airport.lon
        content = [c_type, c_name, c_lat, c_lon]

        if param in PARAMS.keys():
            return content[PARAMS.get(param)]
        else:
            print('ERROR:: there is not such search parameter')
            return None
    except:
        return None


def radius(fi):
    return RP / (cos(fi)**2 + (sin(fi)*RP/RE)**2)


def distance(search_session, start_point_id, end_point_id):
    try:
        start_coords = search(search_session, start_point_id, 'coords')
        end_coords = search(search_session, end_point_id, 'coords')

        r = (radius(start_coords[0])+radius(end_coords[0]))/2

        start_coords[0], start_coords[1] = radians(start_coords[0]), radians(start_coords[1])
        end_coords[0], end_coords[1] = radians(end_coords[0]), radians(end_coords[1])

        a = cos(start_coords[0])*cos(end_coords[0])*sin((start_coords[1]-end_coords[1])/2)**2
        b = cos(start_coords[0]-end_coords[0]) - cos(start_coords[0])*cos(end_coords[0])*sin((start_coords[1]-end_coords[1])/2)**2
        c = sin(start_coords[0]-end_coords[0])

        return round(r*atan2(sqrt(4*a*b+c**2), b-a)/1000)
    except:
        return None

