AIRPORTS_DATA=/root/deploy/airport-codes.csv
if ! [ -f "$AIRPORTS_DATA" ]; then
  wget --quiet https://datahub.io/core/airport-codes/r/airport-codes.csv -O $AIRPORTS_DATA;
fi


if [ "$(docker network ls | grep bot_network)" == "" ]; then
  docker network create bot_network
fi


cd /root/deploy


(docker ps -f name=mysqlc | grep mysqlc > /dev/null )  && docker stop mysqlc

sleep 2

docker run \
    --rm \
    --name=mysqlc \
    --publish 3306:3306 \
    --network bot_network \
    --env MYSQL_ROOT_HOST='%' \
    --env MYSQL_ROOT_PASSWORD='root' \
    --volume "$(pwd)/data:/var/lib/mysql" \
    --detach \
    mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

while ! mysqladmin ping -h "127.0.0.1" -P 3306 ; do
  sleep 1
done
echo 'database is ready'

(docker ps -f name=pythonc | grep pythonc > /dev/null )  && docker stop pythonc

sleep 2

docker run \
    --rm \
    --name pythonc \
    --network bot_network \
    --volume "$PWD:/usr/src/myapp" \
    --workdir /usr/src/myapp \
    python:3.9.2 \
    bash -c "python -m venv dockerenv && source dockerenv/bin/activate && ls . && pip install -r requirements.txt \
            && python -u data_import.py && python -u bot.py"

